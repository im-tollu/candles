# Candles service

This service connects to a tick provider and serves minute candles downstream.

## Build

To build this project, you need JDK 1.8, Scala 2.12+ and sbt 1.1+

Run `sbt clean assembly` from the project's root folder.

After the build succeeds, you can find generated binary at `./target/scala-2.12/candles-assembly`

## Run

From the project root folder run `./target/scala-2.12/candles-assembly`

You can use ENV vars to set up the service's config:

* CANDLES_UPSTREAM_HOST
* CANDLES_UPSTREAM_PORT
* CANDLES_LISTEN_HOST
* CANDLES_LISTEN_PORT

The service tries to establish a connection with upstream tick provider using exponential backoff strategy, making a dozen attempts.