import Dependencies._
import sbt.Keys.mainClass
import sbtassembly.AssemblyPlugin.defaultUniversalScript

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.6",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "candles",
    libraryDependencies ++= dependencies,
    mainClass in (Compile, run) := Some("candles.app.AppRunner"),
    mainClass in assembly := Some("candles.app.AppRunner"),
    assemblyJarName in assembly := "candles-assembly",
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(prependShellScript = Some(defaultUniversalScript(shebang = false)))
  )

val dependencies = Seq(
  "com.typesafe.akka" %% "akka-stream" % "2.5.16",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.16" % Test,
  scalaTest % Test
)
