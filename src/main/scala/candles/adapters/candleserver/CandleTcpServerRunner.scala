package candles.adapters.candleserver

import akka.actor.{ActorRef, ActorSystem}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source, Tcp}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.util.ByteString
import candles.app.CandlesConfig.CandleServerConfig
import candles.core.CandleCachingProxyPublisher
import candles.core.CandleCachingProxyPublisher.CandlePublisherRef
import candles.vocabulary.Candle

import scala.concurrent.Future

object CandleTcpServerRunner {
  def run(actorSystem: ActorSystem, config: CandleServerConfig, candlePublisher: CandlePublisherRef): Future[Tcp.ServerBinding] = {
    implicit val mat: ActorMaterializer = ActorMaterializer.create(actorSystem)

    val candleSource: Source[Candle, ActorRef] = Source.actorRef(config.candleBufferSize, OverflowStrategy.dropHead)
    val byteStringToCandles = Flow.fromSinkAndSourceCoupledMat(Sink.ignore, candleSource)(Keep.right)
    val connectionFlow = byteStringToCandles
                         .map(candle => s"${Format.format(candle)}\n")
                         .map(ByteString.apply)
    def runConnection(conn: Tcp.IncomingConnection): Unit = {
      val sourceRef = conn.handleWith(connectionFlow)
      val subscription = CandleCachingProxyPublisher.Subscription(sourceRef)
      candlePublisher.subscribe(subscription)
    }
    def server = {
      Tcp()(actorSystem)
      .bind(config.host, config.port)
      .to(Sink.foreach(runConnection))
    }

    server.run
  }

}
