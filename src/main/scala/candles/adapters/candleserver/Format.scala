package candles.adapters.candleserver

import java.time.format.DateTimeFormatter.ISO_INSTANT

import candles.vocabulary._

object Format {
  def format(candle: Candle): String = {
    val ticker = s""""ticker": ${format(candle.ticker)}"""
    val timestamp = s""""timestamp": ${format(candle.minute)}"""
    val open = s""""open": ${format(candle.open)}"""
    val high = s""""high": ${format(candle.high)}"""
    val low = s""""low": ${format(candle.low)}"""
    val close = s""""close": ${format(candle.close)}"""
    val volume = s""""volume": ${format(candle.volume)}"""
    s"""{$ticker, $timestamp, $open, $high, $low, $close, $volume}"""
  }

  def format(ticker: Ticker): String = {
    s""""${ticker.symbol}""""
  }
  def format(minute: Minute): String = {
    s""""${ISO_INSTANT.format(minute.toInstant())}""""
  }
  def format(open: Open): String = {
    s"${format(open.price)}"
  }

  def format(close: Close): String = {
    s"${format(close.price)}"
  }

  def format(low: Low): String = {
    s"${format(low.price)}"
  }

  def format(high: High): String = {
    s"${format(high.price)}"
  }

  def format(price: Price): String = {
    s"${format(price.amount)}"
  }

  def format(volume: Volume): String = {
    s"${volume.amount}"
  }
  def format(decimal: BigDecimal): String = {
    s"$decimal"
  }
}
