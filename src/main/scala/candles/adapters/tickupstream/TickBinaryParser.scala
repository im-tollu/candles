package candles.adapters.tickupstream

import java.time.Instant

import akka.util.ByteString
import candles.vocabulary._

object TickBinaryParser {
  def parse(binaryTick: ByteString): Tick = {
    val byteBuffer = binaryTick.asByteBuffer
    def getTicker(tickerLen: Int): Ticker = {
      val chars = binaryTick.drop(tickerOffset).take(tickerLen).map(_.toChar).toArray
      Ticker(String.valueOf(chars))
    }
    def getTimestamp: Timestamp = {
      val epochMilli = byteBuffer.getLong(timestampIndex)
      val instant = Instant.ofEpochMilli(epochMilli)
      Timestamp(instant)
    }
    def getPrice(tickerLen: Int): Price = {
      val priceDouble = byteBuffer.getDouble(priceIndex(tickerLen))
      val amount = BigDecimal(priceDouble)
      Price(amount)
    }
    def getSize(tickerLen: Int): Size = {
      val amount = byteBuffer.getInt(sizeIndex(tickerLen))
      Size(amount)
    }
    val tickerLen = byteBuffer.getShort(tickerLenIndex)
    Tick(getTicker(tickerLen), getTimestamp, getPrice(tickerLen), getSize(tickerLen))
  }

  // [ LEN:2 ] [ TIMESTAMP:8 ] [ TICKER_LEN:2 ] [ TICKER:TICKER_LEN ] [ PRICE:8 ] [ SIZE:4 ]
  //    LEN: длина последующего сообщения (целое, 2 байта)
  //    TIMESTAMP: дата и время события (целое, 8 байт, milliseconds since epoch)
  //    TICKER_LEN: длина биржевого тикера (целое, 2 байта)
  //    TICKER: биржевой тикер (ASCII, TICKER_LEN байт)
  //    PRICE: цена сделки (double, 8 байт)
  //    SIZE: объем сделки (целое, 4 байта)
  private val timestampIndex = 2
  private val tickerLenIndex = 10
  private val tickerOffset = 12
  private def priceIndex(tickerLen: Int) = 12 + tickerLen
  private def sizeIndex(tickerLen: Int) = 20 + tickerLen
}
