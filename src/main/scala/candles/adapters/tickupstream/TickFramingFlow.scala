package candles.adapters.tickupstream

import java.nio.ByteOrder

import akka.NotUsed
import akka.stream.scaladsl.{Flow, Framing}
import akka.util.ByteString
import candles.vocabulary._

object TickFramingFlow {
  def getFlow: Flow[ByteString, Tick, NotUsed] = {
    framingFlow.via(bytesToTickFlowConverter)
  }

  private val lenIndex = 0
  private val lenLength = 2
  private val framingFlow = Framing.lengthField(
    fieldLength = lenLength,
    fieldOffset = lenIndex,
    maximumFrameLength = 32,
    // Endian установлен эмпирически
    byteOrder = ByteOrder.BIG_ENDIAN,
    // Поскольку поле LEN содержит длину *последуюушего* сообщения, используется данная формула для
    // вычисления длины всего фрейма: к значению поля LEN прибавляется длина самого поля LEN
    computeFrameSize = (_, len) => len + lenLength
  )
  private val bytesToTickFlowConverter = Flow[ByteString].map(TickBinaryParser.parse)
}
