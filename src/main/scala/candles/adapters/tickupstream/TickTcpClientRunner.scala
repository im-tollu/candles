package candles.adapters.tickupstream

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl.{RestartSource, Sink, Source, Tcp}
import akka.util.ByteString
import akka.{Done, NotUsed}
import candles.app.CandlesConfig.TickClientConfig
import candles.vocabulary.Tick

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Future, Promise}
import scala.util.Failure

object TickTcpClientRunner {

  def run(actorSystem: ActorSystem, config: TickClientConfig, sink: Sink[Tick, NotUsed]): Future[Done] = {
    val connFlow = Tcp()(actorSystem).outgoingConnection(config.host, config.port)
    var restartCountdown = 12
    val done = Promise[Done]
    val connSourceWithRestart =
      RestartSource.withBackoff(100.millis, 4.seconds, 0.1) {
        () => {
          restartCountdown = restartCountdown - 1
          if (restartCountdown <= 0) {
            done.complete(Failure(new RuntimeException("Reached max attempts to connect to upstream")))
          }
          Source.single(ByteString.empty).via(connFlow)
        }
      }
    val tickClient = connSourceWithRestart
                     .via(TickFramingFlow.getFlow)
                     .to(sink)

    implicit val mat: ActorMaterializer = ActorMaterializer.create(actorSystem)
    tickClient.run()
    done.future
  }

}
