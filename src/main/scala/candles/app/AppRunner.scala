package candles.app

import akka.NotUsed
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.stream.scaladsl.Sink
import akka.util.Timeout
import candles.adapters.candleserver.CandleTcpServerRunner
import candles.adapters.tickupstream.TickTcpClientRunner
import candles.app.CandlesConfig.{CandleServerConfig, CoreConfig, TickClientConfig}
import candles.core.CoreSupervisor
import candles.core.CoreSupervisor.{GetRunningCore, RunningCore}
import candles.vocabulary.Tick

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{Duration, DurationInt}

object AppRunner extends App {
  val config = ConfigReader.readConfig()
  val actorSystem = ActorSystem("CandlesApp")
  val core = runCore(actorSystem, config.core)
  val clientFuture = runUpstreamClient(actorSystem, config.upstream, core.sink)
  val server = CandleTcpServerRunner.run(actorSystem, config.candleServer, core.candleProxy)
  printPortAndHost(config.candleServer)

  private def runCore(actorSystem: ActorSystem, config: CoreConfig): RunningCore = {
    val coreSupervisor = actorSystem.actorOf(CoreSupervisor.props(config))
    implicit val askTimeout: Timeout = Timeout.apply(1.seconds)
    val coreFuture = (coreSupervisor ? GetRunningCore).mapTo[RunningCore]
    Await.result(coreFuture, Duration.Inf)
  }

  private def runUpstreamClient(actorSystem: ActorSystem, config: TickClientConfig, tickSink: Sink[Tick, NotUsed]) = {
    val clientFuture = TickTcpClientRunner.run(actorSystem, config, tickSink)
    clientFuture.failed.foreach(_ => System.exit(1))
    clientFuture
  }

  private def printPortAndHost(config: CandleServerConfig): Unit = {
    println(s"Waiting for TCP connection on ${config.host}:${config.port}...")
  }
}
