package candles.app

import candles.app.CandlesConfig.{CandleServerConfig, CoreConfig, TickClientConfig}

case class CandlesConfig(core: CoreConfig, upstream: TickClientConfig, candleServer: CandleServerConfig)

object CandlesConfig {

  case class CoreConfig(cacheMinutes: Long)

  case class TickClientConfig(host: String, port: Int)

  case class CandleServerConfig(host: String, port: Int, candleBufferSize: Int)

}
