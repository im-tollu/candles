package candles.app

import candles.app.CandlesConfig.{CandleServerConfig, CoreConfig, TickClientConfig}
import com.typesafe.config.{Config, ConfigFactory}

object ConfigReader {
  def readConfig(): CandlesConfig = {
    val rawConf = ConfigFactory.load()
    val rawCandleConf = rawConf.getConfig("candles")
    val coreConfig = readCoreConfig(rawCandleConf)
    val clientConfig = readTickClientConfig(rawCandleConf)
    val serverConfig = readCandleServerConfig(rawCandleConf)
    CandlesConfig(coreConfig, clientConfig, serverConfig)
  }

  private def readCoreConfig(rawCandleConf: Config): CoreConfig = {
    val rawCoreConfig = rawCandleConf.getConfig("core")
    val cacheMinutes = rawCoreConfig.getLong("cacheMinutes")
    CoreConfig(cacheMinutes)
  }

  private def readTickClientConfig(rawCandleConf: Config): TickClientConfig = {
    val rawClientConfig = rawCandleConf.getConfig("tickClient")
    val host = rawClientConfig.getString("host")
    val port = rawClientConfig.getInt("port")
    TickClientConfig(host, port)
  }

  private def readCandleServerConfig(rawCandleConf: Config): CandleServerConfig = {
    val rawServerConfig = rawCandleConf.getConfig("candleServer")
    val host = rawServerConfig.getString("host")
    val port = rawServerConfig.getInt("port")
    val bufferSize = rawServerConfig.getInt("candleBufferSize")
    CandleServerConfig(host, port, bufferSize)
  }
}
