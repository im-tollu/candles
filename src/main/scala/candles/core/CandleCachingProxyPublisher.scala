package candles.core

import akka.actor.{Actor, ActorRef, Props, Timers}
import candles.core.CandleCachingProxyPublisher.{Complete, PurgeOldCandles, Subscription, Unsubscribe}
import candles.vocabulary.Candle

import scala.concurrent.duration.DurationInt

class CandleCachingProxyPublisher(val cacheMinutes: Long) extends Actor with Timers {
  private var buffer = List.empty[Candle]
  private var subscribers = List.empty[Subscription]

  timers.startPeriodicTimer("PurgeOldCandles", PurgeOldCandles, 30.second)

  override def receive(): PartialFunction[Any, Unit] = {
    case candle: Candle => receiveCandle(candle)
    case PurgeOldCandles => purgeOldCandles()
    case subscribe: Subscription => receiveSubscribe(subscribe)
    case Unsubscribe(subscription) => receiveUnsubscribe(subscription)
    case Complete => receiveComplete()
  }

  private def receiveCandle(candle: Candle): Unit = {
    buffer = candle :: buffer
    subscribers.foreach(_.notify(candle))
  }

  private def purgeOldCandles(): Unit = {
    buffer = buffer.filterNot(_.isOlderThan(cacheMinutes))
  }

  private def receiveSubscribe(subscription: Subscription): Unit = {
    subscribers = subscription :: subscribers
    context.watchWith(subscription.subscriber, Unsubscribe(subscription))
    buffer.sorted(Candle.timeOrdering).foreach(subscription.notify(_))
  }

  private def receiveUnsubscribe(subscription: Subscription): Unit = {
    subscribers = subscribers.filterNot(_ == subscription)
  }

  private def receiveComplete(): Unit = {
    context.system.terminate()
  }
}

object CandleCachingProxyPublisher {
  def props(cacheMinutes: Long): Props = {
    Props(
      classOf[CandleCachingProxyPublisher],
      cacheMinutes
    )
  }

  case class CandlePublisherRef(ref: ActorRef) {
    def subscribe(subscription: Subscription): Unit = {
      ref.tell(subscription, ActorRef.noSender)
    }

    def unsubscribe(subscription: Subscription): Unit = {
      ref.tell(Unsubscribe(subscription), ActorRef.noSender)
    }
  }

  case object PurgeOldCandles

  case object Complete

  case class Unsubscribe(subscription: Subscription)

  case class Subscription(subscriber: ActorRef) {
    def notify(message: Any): Unit = {
      subscriber.tell(message, ActorRef.noSender)
    }
  }

}
