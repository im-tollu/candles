package candles.core

import akka.NotUsed
import akka.actor.{Actor, ActorRef, ActorRefFactory, AllForOneStrategy, Props, SupervisorStrategy}
import akka.stream.scaladsl.{Flow, Sink}
import candles.app.CandlesConfig.CoreConfig
import candles.core.CandleCachingProxyPublisher.CandlePublisherRef
import candles.core.CoreSupervisor.{GetRunningCore, RunningCore, tickSink}
import candles.vocabulary.Tick

import scala.concurrent.duration.DurationInt

class CoreSupervisor(val config: CoreConfig) extends Actor {
  val candlePublisher: ActorRef = context.actorOf(CandleCachingProxyPublisher.props(config.cacheMinutes))
  val candlePublisherRef = CandlePublisherRef(candlePublisher)
  val runningCore = RunningCore(tickSink(candlePublisher), candlePublisherRef)
  override def receive: Receive = {
    case GetRunningCore => sender().tell(runningCore, ActorRef.noSender)
  }
  override def supervisorStrategy: SupervisorStrategy = {
    val decider: SupervisorStrategy.Decider = {
      case _ => SupervisorStrategy.escalate
    }
    AllForOneStrategy(0, 0.seconds, loggingEnabled = true)(decider)
  }
}

object CoreSupervisor {
  def props(config: CoreConfig): Props = Props(classOf[CoreSupervisor], config)

  case object GetRunningCore

  case class RunningCore(sink: Sink[Tick, NotUsed], candleProxy: CandlePublisherRef)

  private def tickSink(to: ActorRef): Sink[Tick, NotUsed] = {
    val sink = Sink.actorRef(to, CandleCachingProxyPublisher.Complete)
    val ticksToCandles = Flow.fromGraph(new TicksToCandlesFlowConverter)
    ticksToCandles.to(sink)
  }

  def run(actorContext: ActorRefFactory, config: CoreConfig): RunningCore = {
    val candlePublisher = actorContext.actorOf(CandleCachingProxyPublisher.props(config.cacheMinutes), "CandleCache")
    val candlePublisherRef = CandlePublisherRef(candlePublisher)
    val sink = tickSink(candlePublisher)
    RunningCore(sink, candlePublisherRef)
  }
}
