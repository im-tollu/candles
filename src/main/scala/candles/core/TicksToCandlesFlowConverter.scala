package candles.core

import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import candles.vocabulary.{Candle, Minute, Tick}

import scala.collection.mutable

class TicksToCandlesFlowConverter extends GraphStage[FlowShape[Tick, Candle]] {
  val out: Outlet[Candle] = Outlet("CandleOut")
  val in: Inlet[Tick] = Inlet("TickIn")
  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = {
    new GraphStageLogic(shape) {
      private var currentMinute: Minute = _
      private var ticks: List[Tick] = List.empty
      private val candleQueue = new mutable.Queue[Candle]()
      private var demand = 0
      private var isFinished = false

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          val tick = grab(in)
          if (currentMinute == tick.minute) {
            ticks = tick :: ticks
          } else {
            val candles: List[Candle] = toCandles(ticks)
            candles.foreach(candleQueue.enqueue(_))
            currentMinute = tick.minute
            ticks = List(tick)
          }
          processDemand()
        }

        override def onUpstreamFinish(): Unit = {
          val candles: List[Candle] = toCandles(ticks)
          candles.foreach(candleQueue.enqueue(_))
          ticks = List.empty
          isFinished = true
          processDemand()
        }

      })

      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
          demand = demand + 1
          processDemand()
        }
        override def onDownstreamFinish(): Unit = cancel(in)
      })

      def toCandles(ticks: List[Tick]): List[Candle] = {
        ticks
        .groupBy(_.ticker)
        .map(kv => Candle(kv._1, currentMinute, kv._2))
        .toList
      }

      def processDemand(): Unit = {
        if (demand > 0 && candleQueue.nonEmpty) {
          val candle = candleQueue.dequeue()
          push(out, candle)
          demand = demand - 1
        }
        if (!isFinished && !hasBeenPulled(in)) {
          pull(in)
        }
        if (isFinished && candleQueue.isEmpty) {
          complete(out)
        }
      }

    }
  }
  override def shape: FlowShape[Tick, Candle] = FlowShape(in, out)
}
