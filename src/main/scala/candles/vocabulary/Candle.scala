package candles.vocabulary

case class Candle(
  ticker: Ticker,
  minute: Minute,
  ticks: List[Tick]
) {
  require(ticks.forall(_.ticker == ticker), "The candle's ticks must have the same ticker!")
  require(ticks.forall(_.timestamp.minute == minute), "The candle's ticks must belong to the same minute!")

  val open: Open = Open(ticks.min(Tick.timestampOrdering).price)
  val close: Close = Close(ticks.max(Tick.timestampOrdering).price)
  val high: High = High(ticks.max(Tick.priceOrdering).price)
  val low: Low = Low(ticks.min(Tick.priceOrdering).price)
  val volume: Volume = Volume(ticks.map(_.size))

  def isOlderThan(expirationMinutes: Long): Boolean = {
    val now = Timestamp.now()
    val currentMinute = now.minute
    val expiresAt = minute.addMinutes(expirationMinutes)
    currentMinute.isAfter(expiresAt)
  }
}

object Candle {
  val timeOrdering: Ordering[Candle] = Ordering.by[Candle, Minute](_.minute)(Minute.timestampOrdering)
}

