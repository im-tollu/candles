package candles.vocabulary

case class Close(price: Price)

object Close {
  def apply(tick: Tick): Close = Close(tick.price)
}