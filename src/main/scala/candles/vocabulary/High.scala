package candles.vocabulary

case class High(price: Price)

object High {
  def apply(tick: Tick): High = High(tick.price)
}