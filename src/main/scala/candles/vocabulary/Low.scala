package candles.vocabulary

case class Low(price: Price)

object Low {
  def apply(tick: Tick): Low = Low(tick.price)

}