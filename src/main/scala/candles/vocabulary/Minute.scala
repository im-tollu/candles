package candles.vocabulary

import java.time._

case class Minute(date: LocalDate, hour: Int, minute: Int) {
  def toLocalDateTime(): LocalDateTime = {
    val time = LocalTime.of(hour, minute)
    LocalDateTime.of(date, time)
  }
  def toInstant(): Instant = {
    toLocalDateTime().toInstant(ZoneOffset.UTC)
  }
  def addMinutes(minutes: Long): Minute = {
    val dateTime = toLocalDateTime()
    val newDateTime = dateTime.plusMinutes(minutes)
    Minute(newDateTime.toLocalDate, newDateTime.getHour, newDateTime.getMinute)
  }
  def isAfter(other: Minute): Boolean = {
    val thisDT = toLocalDateTime()
    val otherDT = other.toLocalDateTime()
    thisDT.isAfter(otherDT)
  }
}

object Minute {
  val timestampOrdering: Ordering[Minute] = Ordering.by(_.toInstant())
}
