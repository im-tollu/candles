package candles.vocabulary

case class Open(price: Price)

object Open {
  def apply(tick: Tick): Open = Open(tick.price)
}