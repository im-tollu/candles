package candles.vocabulary

case class Price(amount: BigDecimal)

object Price {
  implicit val ordering: Ordering[Price] = Ordering.by(_.amount)
  def sum(prices: Traversable[Price]): Price = Price(prices.map(_.amount).sum)
  def apply(s: String): Price = {
    Price(BigDecimal(s))
  }
}
