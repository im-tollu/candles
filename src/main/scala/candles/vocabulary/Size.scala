package candles.vocabulary

case class Size(amount: Int)

object Size {
  def sum(sizes: Traversable[Size]): Size = Size(sizes.map(_.amount).sum)
  def apply(s: String): Size = {
    Size(Integer.parseInt(s))
  }
}
