package candles.vocabulary

case class Tick(
  ticker: Ticker,
  timestamp: Timestamp,
  price: Price,
  size: Size
) {
  def minute: Minute = timestamp.minute
}

object Tick {
  val timestampOrdering: Ordering[Tick] = Ordering.by(_.timestamp)
  val priceOrdering: Ordering[Tick] = Ordering.by(_.price)
}
