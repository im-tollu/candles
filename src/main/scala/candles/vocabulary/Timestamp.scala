package candles.vocabulary

import java.time.ZoneOffset.UTC
import java.time.{Instant, LocalDateTime}

case class Timestamp(instant: Instant) {
  val minute: Minute = {
    val dateTime = LocalDateTime.ofInstant(instant, UTC)
    Minute(dateTime.toLocalDate, dateTime.getHour, dateTime.getMinute)
  }
}

object Timestamp {
  implicit val ordering: Ordering[Timestamp] = Ordering.by(_.instant)
  def now(): Timestamp = Timestamp(Instant.now())
  def apply(s: String): Timestamp = {
    Timestamp(Instant.parse(s))
  }
}