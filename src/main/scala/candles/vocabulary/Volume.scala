package candles.vocabulary

case class Volume(amount: Int)

object Volume{
  def apply(sizes: Traversable[Size]): Volume = Volume(Size.sum(sizes).amount)
}