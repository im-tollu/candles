package candles

import java.time.LocalDate

import akka.util.ByteString
import candles.vocabulary._

object Examples {

  case class BytesAndTick(bytes: ByteString, tick: Tick)

  val bytesAndTicks = Seq(
    BytesAndTick(
      ByteString(0, 26, 0, 0, 1, 101, -66, -95, -71, 53, 0, 4, 71, 79, 79, 71, 64, 87, 99, 51, 51, 51, 51, 51, 0, 0, 25,
        0),
      Tick(Ticker("GOOG"), Timestamp("2018-09-09T13:59:54.421Z"), Price("93.55"), Size("6400"))
    ),
    BytesAndTick(
      ByteString(0, 26, 0, 0, 1, 101, -66, -95, -66, -30, 0, 4, 71, 79, 79, 71, 64, 91, 102, 102, 102, 102, 102, 102, 0,
        0, 8, -4),
      Tick(Ticker("GOOG"), Timestamp("2018-09-09T13:59:55.874Z"), Price("109.6"), Size("2300"))
    ),
    BytesAndTick(
      ByteString(0, 26, 0, 0, 1, 101, -66, -95, -59, 32, 0, 4, 71, 79, 79, 71, 64, 90, 80, 0, 0, 0, 0, 0, 0, 0, 30,
        -36),
      Tick(Ticker("GOOG"), Timestamp("2018-09-09T13:59:57.472Z"), Price("105.25"), Size("7900"))
    ),
    BytesAndTick(
      ByteString(0, 25, 0, 0, 1, 101, -66, -95, -49, 102, 0, 3, 83, 80, 89, 64, 90, 9, -103, -103, -103, -103, -102, 0,
        0, 35, -16),
      Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:00.102Z"), Price("104.15"), Size("9200"))
    ),
    BytesAndTick(
      ByteString(0, 26, 0, 0, 1, 101, -66, -95, -40, 104, 0, 4, 71, 79, 79, 71, 64, 86, -77, 51, 51, 51, 51, 51, 0, 0,
        7, 108),
      Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:02.408Z"), Price("90.8"), Size("1900"))
    ),
    BytesAndTick(
      ByteString(0, 25, 0, 0, 1, 101, -66, -95, -38, 33, 0, 3, 83, 80, 89, 64, 90, 54, 102, 102, 102, 102, 102, 0, 0,
        32, -48),
      Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:02.849Z"), Price("104.85"), Size(8400))
    ),
    BytesAndTick(
      ByteString(0, 25, 0, 0, 1, 101, -66, -95, -32, -44, 0, 3, 83, 80, 89, 64, 86, -39, -103, -103, -103, -103, -102,
        0, 0, 18, 92),
      Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:04.564Z"), Price("91.4"), Size("4700"))
    ),
    BytesAndTick(
      ByteString(0, 25, 0, 0, 1, 101, -66, -95, -30, -56, 0, 3, 83, 80, 89, 64, 90, 25, -103, -103, -103, -103, -102, 0,
        0, 8, -4),
      Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:05.064Z"), Price("104.4"), Size("2300"))
    ),
    BytesAndTick(
      ByteString(0, 26, 0, 0, 1, 101, -66, -95, -26, 58, 0, 4, 65, 65, 80, 76, 64, 89, -122, 102, 102, 102, 102, 102, 0,
        0, 12, -128),
      Tick(Ticker("AAPL"), Timestamp("2018-09-09T14:00:05.946Z"), Price("102.1"), Size("3200"))
    ),
    BytesAndTick(
      ByteString(0, 26, 0, 0, 1, 101, -66, -95, -25, -19, 0, 4, 65, 65, 80, 76, 64, 89, 41, -103, -103, -103, -103,
        -102, 0, 0, 26, 44),
      Tick(Ticker("AAPL"), Timestamp("2018-09-09T14:00:06.381Z"), Price("100.65"), Size("6700"))
    ),
    BytesAndTick(
      ByteString(0, 26, 0, 0, 1, 101, -66, -95, -13, 117, 0, 4, 71, 79, 79, 71, 64, 88, 96, 0, 0, 0, 0, 0, 0, 0, 34,
        96),
      Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:09.333Z"), Price("97.5"), Size("8800"))
    )
  )

  val candles = Seq(
    Candle(
      Ticker("GOOG"),
      Minute(LocalDate.parse("2018-09-09"), 13, 59),
      List(
        Tick(Ticker("GOOG"), Timestamp("2018-09-09T13:59:57.472Z"), Price("105.25"), Size("7900")),
        Tick(Ticker("GOOG"), Timestamp("2018-09-09T13:59:55.874Z"), Price("109.6"), Size("2300")),
        Tick(Ticker("GOOG"), Timestamp("2018-09-09T13:59:54.421Z"), Price("93.55"), Size("6400"))
      )
    ),
    Candle(
      Ticker("SPY"),
      Minute(LocalDate.parse("2018-09-09"), 14, 0),
      List(
        Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:05.064Z"), Price("104.4"), Size("2300")),
        Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:04.564Z"), Price("91.4"), Size("4700")),
        Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:02.849Z"), Price("104.85"), Size("8400")),
        Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:00.102Z"), Price("104.15"), Size("9200"))
      )
    ),
    Candle(
      Ticker("AAPL"),
      Minute(LocalDate.parse("2018-09-09"), 14, 0),
      List(
        Tick(Ticker("AAPL"), Timestamp("2018-09-09T14:00:06.381Z"), Price("100.65"), Size("6700")),
        Tick(Ticker("AAPL"), Timestamp("2018-09-09T14:00:05.946Z"), Price("102.1"), Size("3200"))
      )
    ),
    Candle(
      Ticker("GOOG"),
      Minute(LocalDate.parse("2018-09-09"), 14, 0),
      List(
        Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:09.333Z"), Price("97.5"), Size("8800")),
        Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:02.408Z"), Price("90.8"), Size("1900"))
      )
    )
  )
}
