package candles.adapters.tickupstream

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.TestKit
import akka.util.ByteString
import candles.Examples.bytesAndTicks
import candles.vocabulary._
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

class FramingAndParsingSpec
  extends TestKit(ActorSystem("CandlesTest"))
          with FlatSpecLike
          with Matchers
          with BeforeAndAfterAll {
  implicit val mat: ActorMaterializer = ActorMaterializer.create(system)

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "Byte stream from test tick provider" should "be correctly parsed into Ticks" in {
    val bytes = bytesAndTicks.map(_.bytes).reduceLeft((s1, s2) => s1 ++ s2)
    val expectedTicks = bytesAndTicks.map(_.tick)
    val chunkSize = 12
    val byteChunks = bytes.grouped(chunkSize).toList
    val source = Source.apply[ByteString](byteChunks)
    val sinkUnderTest = Sink.fold[Seq[Tick], Tick](Seq.empty)((seq, tick) => seq :+ tick)

    val futureResult = source.via(TickFramingFlow.getFlow).runWith(sinkUnderTest)
    val actualTicks = Await.result(futureResult, 1.second)

    assertResult(expectedTicks)(actualTicks)
  }

}
