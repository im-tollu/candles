package candles.core

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.TestKit
import candles.Examples
import candles.vocabulary.{Candle, Tick}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

class TicksToCandlesSpec
  extends TestKit(ActorSystem("CandlesTest"))
          with FlatSpecLike
          with Matchers
          with BeforeAndAfterAll {
  implicit val mat: ActorMaterializer = ActorMaterializer.create(system)

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "Ticks to candles converter" should "split ticks by minute and calculate candles" in {
    val ticks = Examples.bytesAndTicks.map(_.tick).toList
    val expectedCandles = Examples.candles.toSet
    val source = Source.apply[Tick](ticks)
    val sinkUnderTest = Sink.fold[Set[Candle], Candle](Set.empty)((set, candle) => set + candle)

    val futureResult = source.via(new TicksToCandlesFlowConverter()).runWith(sinkUnderTest)
    val actualCandles = Await.result(futureResult, 1.second)

    assertResult(expectedCandles)(actualCandles)
  }
}
