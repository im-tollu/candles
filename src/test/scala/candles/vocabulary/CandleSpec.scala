package candles.vocabulary

import java.time.LocalDate

import org.scalatest.{FlatSpec, Matchers}

class CandleSpec extends FlatSpec with Matchers {

  "Candles" should "be checked for equality by value" in {
    val candle1 = Candle(
      Ticker("GOOG"),
      Minute(LocalDate.parse("2018-09-09"), 14, 0),
      List(
        Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:09.333Z"), Price("97.5"), Size("8800")),
        Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:02.408Z"), Price("90.8"), Size("1900"))
      )
    )
    val candle2 = Candle(
      Ticker("GOOG"),
      Minute(LocalDate.parse("2018-09-09"), 14, 0),
      List(
        Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:09.333Z"), Price("97.5"), Size("8800")),
        Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:02.408Z"), Price("90.8"), Size("1900"))
      )
    )

    assert(candle1 == candle2)
  }

  it should "throw if a candle's ticker doesn't match its ticks' ticker" in {
    assertThrows[IllegalArgumentException] {
      Candle(
        Ticker("AAPL"),
        Minute(LocalDate.parse("2018-09-09"), 14, 0),
        List(
          Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:09.333Z"), Price("97.5"), Size("8800")),
          Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:02.408Z"), Price("90.8"), Size("1900"))
        )
      )
    }
  }

  it should "throw if a candle's minute doesn't match its ticks' minute" in {
    assertThrows[IllegalArgumentException] {
      Candle(
        Ticker("GOOG"),
        Minute(LocalDate.parse("2018-09-09"), 14, 1),
        List(
          Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:09.333Z"), Price("97.5"), Size("8800")),
          Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:02.408Z"), Price("90.8"), Size("1900"))
        )
      )
    }
  }

  it should "throw if there are ticks with different tickers" in {
    assertThrows[IllegalArgumentException] {
      Candle(
        Ticker("GOOG"),
        Minute(LocalDate.parse("2018-09-09"), 14, 0),
        List(
          Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:09.333Z"), Price("97.5"), Size("8800")),
          Tick(Ticker("AAPL"), Timestamp("2018-09-09T14:00:02.408Z"), Price("90.8"), Size("1900"))
        )
      )
    }
  }

  it should "throw if there are ticks with different minutes" in {
    assertThrows[IllegalArgumentException] {
      Candle(
        Ticker("GOOG"),
        Minute(LocalDate.parse("2018-09-09"), 14, 0),
        List(
          Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:00:09.333Z"), Price("97.5"), Size("8800")),
          Tick(Ticker("GOOG"), Timestamp("2018-09-09T14:01:02.408Z"), Price("90.8"), Size("1900"))
        )
      )
    }
  }

  it should "correctly calculate open, close, low and high" in {
    val candle = Candle(
      Ticker("SPY"),
      Minute(LocalDate.parse("2018-09-09"), 14, 0),
      List(
        Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:05.064Z"), Price("104.4"), Size("2300")),
        Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:04.564Z"), Price("91.4"), Size("4700")),
        Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:02.849Z"), Price("104.85"), Size("8400")),
        Tick(Ticker("SPY"), Timestamp("2018-09-09T14:00:00.102Z"), Price("104.15"), Size("9200"))
      )
    )
    val openExpected = Open(Price("104.15"))
    val closeExpected = Close(Price("104.4"))
    val lowExpected = Low(Price("91.4"))
    val highExpected = High(Price("104.85"))

    assert(openExpected == candle.open)
    assert(closeExpected == candle.close)
    assert(highExpected == candle.high)
    assert(lowExpected == candle.low)
  }

}
